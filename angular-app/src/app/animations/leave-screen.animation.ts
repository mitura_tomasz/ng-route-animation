import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes, group } from '@angular/animations';

export const LeaveScreenAnimation = trigger('leaveScreen', 
  [
    state('void', style({ position: 'fixed', top: '50%', left: '-150%', width: '100%', opacity: '0' }))
    state('*', style({ position: 'fixed', top: '50%', width: '100%' })),
    transition(':enter', [
      group([
        animate('1250ms ease-out', style({ transform: 'translateX(150%)' })),
        animate('500ms 750ms ease-out', style({ opacity: '1' }))
      ])
    ]),
    transition(':leave', [
      group([
        animate('1250ms ease-out', style({ transform: 'translateX(150%)'})),
        animate('250ms ease-out', style({ opacity: '0' })),
      ])
    ])
  ]
);
