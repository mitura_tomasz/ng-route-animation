import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export const ShakeAnimation = trigger('shakeAnimation',
  [
    state('invalid', style({})),
    state('unchecked', style({})),
    state('completed', style({ transform: 'translateX(calc(150% + 350px))' })),
    transition('unchecked => invalid', animate(250, keyframes([
      style({ transform: 'translateX(-10%)' }),
      style({ transform: 'translateX(10%)' }),
      style({ transform: 'translateX(10%)' }),
      style({ transform: 'translateX(-10%)' })
    ])))
  ],
);
