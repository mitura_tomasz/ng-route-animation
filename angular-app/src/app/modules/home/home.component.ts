import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { ShakeAnimation } from './../../animations/shake.animation';
import { LeaveScreenAnimation } from './../../animations/leave-screen.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [ShakeAnimation, LeaveScreenAnimation],
  // "host:" Allows to attach events, amimations, all different kinds of things to the component hosting element
  // W naszym przypadku uzywamy router outlet, w zwiazku z tym nie ma miejsca-znacznika gdzie moge 
  // nadac "[@leaveScreen]:"smg", dlatego robi sie to w dekoratorze komponentu
  host: { '[@leaveScreen]': '' } 
})
export class HomeComponent implements OnInit {
  
  homeScreenAnimation: string = 'unchecked';
  name: string = "";
  
  constructor(private _router: Router) { }

  ngOnInit() {
  }
  
  onHomeButtonClick() {  
    this.homeScreenAnimation = (this.name ? "completed" : "invalid");
  }
  
  setBackToUnchecked() {
    if (this.homeScreenAnimation === 'invalid') {
      this.homeScreenAnimation = 'unchecked';
    }
    if (this.homeScreenAnimation === "completed") {
      this._router.navigateByUrl('profile');
    }
  }

}
