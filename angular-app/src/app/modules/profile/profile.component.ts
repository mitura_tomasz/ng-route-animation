import { Component, OnInit } from '@angular/core';

import { LeaveScreenAnimation } from './../../animations/leave-screen.animation';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  animations: [LeaveScreenAnimation],
  host: { '[@leaveScreen]': '' }

})
export class ProfileComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
